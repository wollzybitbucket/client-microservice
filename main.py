from flask import Flask, request, jsonify


app = Flask(__name__)


def make_error(code, message):
    err = {
        'error': {
            'code': code,
            'message': message
        },
        'status': "Error",
        'data': ""
    }
    return jsonify(err), code


@app.route('/client', methods=['GET'])
def client():
    if 'phone' in request.args:

        # load from db

        return jsonify({
            "status": "Ok",
            "data": {
                "id": 1,
                "created_at": "1541183989",
                "updated_at": "1541183989",
                "first_name": "Rafael",
                "last_name": "Shamil",
                "email": "support@wollzy.com",
                "phone": "+7999551122",
                "sex": "man",
                "position": "Proger",
                "birthday": "1541183989"
            }
        })
    elif 'id' in request.args:
        return jsonify({
            "status": "Ok",
            "data": {
                "id": 1,
                "created_at": "1541183989",
                "updated_at": "1541183989",
                "first_name": "Rafael",
                "last_name": "Shamil",
                "email": "support@wollzy.com",
                "phone": "+7999551122",
                "sex": "man",
                "position": "Proger",
                "birthday": "1541183989"
            }
        })
    else:
        return make_error(400, "Phone or id is required.")


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)