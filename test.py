from main import app

import json
from unittest import TestCase, main, TextTestRunner


jwt_header = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXNoYm94X2lkIjoiNjc4NDY1MyIsInBhcnRuZXJfaWQiOiI3MzQ4NjczNDgiLCJ1c2VyX2lkIjoiMzI0MzI1In0.kNyJm8gXyNBPxUCYh_sA4THF9-ok9YV7kja1I0YGKs8'


class Tests(TestCase):
    
    def setUp(self):
        self.cli = app.test_client()

    def test_client(self):
        response = self.cli.get('/client', query_string={'phone':'+71234567899'}, headers = {'Authorization': jwt_header})
        data = response.get_json()
        expected = {
            "status": "Ok",
            "data": {
                "id": 1,
                "created_at": "1541183989",
                "updated_at": "1541183989",
                "first_name": "Rafael",
                "last_name": "Shamil",
                "email": "support@wollzy.com",
                "phone": "+7999551122",
                "sex": "man",
                "position": "Proger",
                "birthday": "1541183989"
            }
        }
        assert data == expected

        response = self.cli.get('/client', query_string={'id':'12333'}, headers = {'Authorization': jwt_header})
        data = response.get_json()
        expected = {
            "status": "Ok",
            "data": {
                "id": 1,
                "created_at": "1541183989",
                "updated_at": "1541183989",
                "first_name": "Rafael",
                "last_name": "Shamil",
                "email": "support@wollzy.com",
                "phone": "+7999551122",
                "sex": "man",
                "position": "Proger",
                "birthday": "1541183989"
            }
        }
        assert data == expected

        response = self.cli.get('/client', headers = {'Authorization': jwt_header})
        data = response.get_json()
        expected = {
            'error': {
                'code': 400,
                'message': "Phone or id is required."
            },
            'status': "Error",
            'data': ""
        }
        assert data == expected

if __name__ == "__main__":
    log_file = 'test_log.txt'
    f = open(log_file, "w")
    runner = TextTestRunner(f)
    main(testRunner=runner)
    f.close()

